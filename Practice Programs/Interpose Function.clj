(def my-strings ["one" "two" "three"])
(interpose ", " my-strings) => ("one" ", " "two" ", " "three")
(apply str (interpose ", " my-strings)) => "one, two, three"
