(re-seq #"[+-]" "-x^7+3x^6+4x^3")
(re-seq #"[^+-]" "x^7+3x^6+4x^3")

(rseq [re-seq #"[+-]+" "-x^7+3x^6+4x^3"])

(re-seq #"[+-]+" "-x^7+3x^6+4x^3")
(re-seq #"[^+-]+" "-x^7+3x^6+4x^3")

(re-seq #"[+-]+" "-x^7+3x^6+4x^3")
(re-seq #"[^+-]+" "-x^7+3x^6+4x^3")
(println seq1)
(println seq2)

(def line "-x^7+3x^6+4x^3")
(re-seq #"[+-]+" line)

(clojure.string/join " " (re-seq #"[+-]+" "-x^7+3x^6+4x^3"))
(clojure.string/join " " (re-seq #"[^+-]+" "-x^7+3x^6+4x^3"))

(seq (clojure.string/reverse (clojure.string/join "" (re-seq #"[+-]+" "-x^7+3x^6+4x^3"))))
(seq (clojure.string/reverse (clojure.string/join "" (re-seq #"[^+-]+" "-x^7+3x^6+4x^3"))))

(defn reverse-recursively [coll]
  (loop [r (rest coll)
         acc (conj () (first coll))]
    (if (= (count r) 0)
      acc
      (recur (rest r) (conj acc (first r))))))
(reverse-recursively (re-seq #"[+-]+" "-x^7+3x^6+4x^3"))
(reverse-recursively (re-seq #"[^+-]+" "-x^7+3x^6+4x^3"))

(interleave (reverse-recursively (re-seq #"[+-]+" "-x^7+3x^6+4x^3")) (reverse-recursively (re-seq #"[^+-]+" "-x^7+3x^6+4x^3")))

 (apply str (interleave (reverse-recursively (re-seq #"[+-]+" "-x^7+3x^6+4x^3")) (reverse-recursively (re-seq #"[^+-]+" "-x^7+3x^6+4x^3"))))