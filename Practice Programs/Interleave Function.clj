(interleave [:a,:b,:c] [1,2,3]) => (:a 1 :b 2 :c 3)

(interleave (repeat "a") [1 2 3]) => ("a" 1 "a" 2 "a" 3)
