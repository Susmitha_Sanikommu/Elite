def divide(x):
    return x // 2
 
def double(x):
    return x * 2
 
def even(x):
    return not x % 2
def russian(a, b):
    result = 0
 
    while a >= 1:
        if not even(a):
            result += b
        a   = divide(a)
        b = double(b)
 
    return result
