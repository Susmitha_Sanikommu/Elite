(defn fibo-lazy [n] (->> [0N 1N] (iterate (fn [[a b]] [b (+ a b)])) (map first) (take n)))
