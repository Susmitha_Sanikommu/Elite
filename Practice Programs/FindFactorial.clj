(defn fact1 ([x] (trampoline (fact1 (dec x) x)))
           ([x a] (if (<= x 1) a #(fact1 (dec x) (*' x a)))))


(fact1 200)