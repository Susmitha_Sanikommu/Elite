(defn reverse-recursively [coll]
  (loop [r (rest coll) 
         acc (conj () (first coll))]
    (if (= (count r) 0)
      acc
      (recur (rest r) (conj acc (first r))))))


(reverse-recursively '(1 2 3 4 5 6))