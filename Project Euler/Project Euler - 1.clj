(loop [i 0 s 0]
    (if (= i 1000) s
       (if (or (= 0 (mod i 3))
              (= 0 (mod i 5))) (recur (inc i) (+ s i))
              (recur (inc i) s))))