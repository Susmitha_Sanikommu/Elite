(defn is-prime? [n]
  (cond (<= n 1) false
        (= n 2) true
        :else (loop [f 2]
                (cond (zero? (rem n f)) false
                      (> f (Math/sqrt n)) true
                      :else (recur (inc f))
		)
	      )
   )
)

(last (take 10 (filter #(is-prime? %) (iterate inc 2))))