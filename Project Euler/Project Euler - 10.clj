(defn is-prime? [n]
  (cond (<= n 1) false
        (= n 2) true
        :else (loop [f 2]
                (cond (zero? (rem n f)) false
                      (> f (Math/sqrt n)) true
                      :else (recur (inc f))
		)
	      )
   )
)

(defn sum-of-primes [limit]
  (reduce + (filter is-prime? (range 2 limit))))