  (for [x (range 1 1000)
        y (range (inc x) 1000)
        z (range (inc y) 1000)
        :when (and (= (+ (* x x)
                         (* y y))
                      (* z z))
                   (= 1000
                      (+ x y z)))]
    (print (* x y z)))