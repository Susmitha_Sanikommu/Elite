(defn fib-seq-lazy []
  (map first (iterate (fn [[a b]] [b (+ a b)]) [1 1])))

(reduce +
   (filter even?(take-while #(< % 4000000)(fib-seq-lazy))))