(defn gcd [n k]
  (loop [a n b k]
    (if (zero? b) a (recur b (mod a b)))
  )
)
(defn lcm [n k]
  (/ (* n k) (gcd n k))
)
(reduce lcm (range 1 21))