(defn euler-21[]

  (defn divisors[x]
    (vec (filter #(= 0 (rem x %1)) (range 1 x))))

  (loop [i 1 numbers #{}] ;; Using a SET to avoid repeated pairs
    (if (= i 10000)
      (reduce + numbers)
      (do
        ;; (println (str i ": " numbers))
        (let [sum-a (reduce + (divisors i))]
          (let [sum-b (reduce + (divisors sum-a))]
            (if
                (and
                 (= i sum-b)
                 (not (= sum-a sum-b)))
              (recur (inc i) (conj (conj numbers i) sum-a))
              (recur (inc i) numbers))))))))