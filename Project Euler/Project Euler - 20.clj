(defn factorial[x]
  (if (<= x 1) 1 (* x  (factorial (dec x))  )))

(defn sum-digits
  [val]
  (reduce +
          (map #(- (int %) 48)
               (str val))))

(sum-digits (factorial 5))
