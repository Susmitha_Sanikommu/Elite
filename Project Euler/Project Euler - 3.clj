(defn get-largest-prime-factor [num]
  	
	(let [q (long (Math/sqrt num))
  
 		factor? (fn [a b] (zero? (rem a b)))]
 
	  		(loop [n num d 2]
	
	   			(cond
		
				   (> d q) n
	
				   (= d n) n

				   (factor? n d) 
				   (recur (/ n d) d)

				   true	(recur n (inc d))

	                        )
	
		)
	
	)

)
