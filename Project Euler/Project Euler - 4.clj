(defn palindrome-number? [n]
  (=(reverse (str n)) (seq (str n))))

  (reduce max (filter palindrome-number?
                      (for [i (range 100 1000) j (range i 1000)] (* i j))))