(defn sequence-step [n]
  (if
    (= n 1) 0
      (if
        (even? n)
        (/ n 2)
        (inc (* 3 n)))))

(defn iterative-sequence [n]
  (when (> n 0)
    (lazy-seq (cons n (iterative-sequence (sequence-step n))))))

